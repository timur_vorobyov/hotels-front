import Navbar from 'components/Navbar/Navbar';
import Header from 'components/Header/Header';
import './HomePage.scss';
import Featured from './Featured/Featured';
import PropertyList from './PropertyList/PropertyList';
import Favorite from './Favorite/Favorite';
import MailSubscription from 'components/MailSubscription/MailSubscription';
import Footer from 'components/Footer/Footer';

const HomePage = () => {
  return (
    <div>
      <Navbar />
      <Header />
      <div className="homeContainer">
        <Featured />
        <h1 className="homeTitle">Browse by propery type</h1>
        <PropertyList />
        <h1 className="homeTitle">Home guests love</h1>
        <Favorite />
        <MailSubscription />
        <Footer />
      </div>
    </div>
  );
};

export default HomePage;
