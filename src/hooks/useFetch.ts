import axios, { AxiosResponse } from 'axios';
import { useEffect, useState } from 'react';

const useFetch = (url: string) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<unknown>(null);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const response: AxiosResponse<any, any> = await axios.get(url);
        setData(response.data);
      } catch (error) {
        setError(error);
      }
      setIsLoading(false);
    };

    fetchData();
  }, [url]);

  const reFetchData = async () => {
    setIsLoading(true);
    try {
      const response: AxiosResponse<any, any> = await axios.get(url);
      setData(response.data);
    } catch (error) {
      setError(error);
    }
    setIsLoading(false);
  };

  return { data, isLoading, error, reFetchData };
};

export default useFetch;
