import HotelDetailsPage from 'pages/HotelDetailsPage/HotelDetailsPage';
import HomePage from './pages/HomePage/HomePage';
import HotelsPage from './pages/HotelsPage/HotelsPage';
import { HOME_ROUTE, HOTELS_ROUTE } from './utils/consts';

export const publicRoutes = [
  {
    path: HOME_ROUTE,
    Component: HomePage
  },
  {
    path: HOTELS_ROUTE,
    Component: HotelsPage
  },
  {
    path: HOTELS_ROUTE + '/:id',
    Component: HotelDetailsPage
  }
];
